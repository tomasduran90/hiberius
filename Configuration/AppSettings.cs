namespace hiberus.Configuration
{
    public class AppSettings
    {
        public string RateUrl { get; set; }
        public string TransactionUrl { get; set; }
        public string SqlConn { get; set; }
    }
}