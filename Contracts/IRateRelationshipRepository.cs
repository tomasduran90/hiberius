using hiberus.Model;

namespace hiberus.Contracts
{
    public interface IRateRelationshipRepository : IRepositoryBase<RateRelationship>
    {
         
    }
}