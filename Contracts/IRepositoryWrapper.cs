namespace hiberus.Contracts
{
    public interface IRepositoryWrapper
    {
        IRateRelationshipRepository RateRelationship { get; }
        ITransactionRepository Transaction { get; }
    }
}