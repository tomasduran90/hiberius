using hiberus.Model;

namespace hiberus.Contracts
{
    public interface ITransactionRepository : IRepositoryBase<Transaction>
    {
         
    }
}