using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hiberus.Contracts;
using Microsoft.AspNetCore.Mvc;
using hiberus.Model;
using hiberus.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

namespace hiberus.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RatesController : ControllerBase
    {
        private IRepositoryWrapper _repoWrapper;
        private readonly ILogger _logger;

        public RatesController(IRepositoryWrapper repoWrapper, ILogger<RatesController> logger)
        {
            _repoWrapper = repoWrapper;
            _logger = logger;
        }

        // GET api/rates
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                 _logger.LogInformation("Fetching all Rates");

                var rates = await _repoWrapper.RateRelationship.FindAll();

                _logger.LogInformation("Fetching all Rates total: ({ID})", rates.Count());

                return Ok(rates);
            }
            catch(Exception ex)
            {
                 _logger.LogError(ex, "Error fetching all Rates");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}