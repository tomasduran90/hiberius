using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hiberus.Configuration;
using hiberus.Contracts;
using hiberus.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace hiberus.Controllers 
{
    [Route ("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private IRepositoryWrapper _repoWrapper;
        private readonly ILogger _logger;

        public TransactionController(IRepositoryWrapper repoWrapper, ILogger<TransactionController> logger)
        {
            _repoWrapper = repoWrapper;
            _logger = logger;
        }

        // GET api/transaction
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                _logger.LogInformation("Fetching all Transactions");

                IEnumerable<Transaction> rvalue = await _repoWrapper.Transaction.FindAll();

                _logger.LogInformation("Fetching all Transaction total: ({ID})", rvalue.Count());

                return Ok(rvalue);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Error fetching all Transactions");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/transaction/5
        [HttpGet("{sku}")]
        public async Task<IActionResult> Get(string sku)
        {
            try
            {
                _logger.LogInformation("Getting Transaction {SKU}", sku);

                var rvalue = await _repoWrapper.Transaction.FindByCondition(x => x.sku == sku);

                if(rvalue == null)
                {
                    _logger.LogWarning("Getting Transaction {SKU} NOT FOUND", sku);
                    return NotFound();
                }

                return Ok(rvalue);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Error fetching {SKU} NOT FOUND", sku);
                return StatusCode(500, "Internal server error");
            }
        }
    }
}