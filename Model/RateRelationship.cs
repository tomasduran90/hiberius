namespace hiberus.Model {
    public class RateRelationship
    {
        public string from { get; set; }
        public string to { get; set; }
        public double rate { get; set; }
    }
}