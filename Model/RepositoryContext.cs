using Microsoft.EntityFrameworkCore;

namespace hiberus.Model {
    public class RepositoryContext : DbContext {
        public RepositoryContext (DbContextOptions options) : base (options) {

        }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            modelBuilder.Entity<RateRelationship> ()
                .HasKey (r => new { r.from, r.to });
        }

        public void TruncateTable<T>()
        {
            var mapping = Model.FindEntityType(typeof(T)).Relational();
            var schema = mapping.Schema;
            var tableName = mapping.TableName;
            Database.ExecuteSqlCommand(string.Format("TRUNCATE TABLE [{0}]", tableName));
        }

        public DbSet<RateRelationship> Rate { get; set; }
        public DbSet<Transaction> Transaction { get; set; }

    }
}