namespace hiberus.Model 
{
    public class Transaction 
    {
        public long? id { get; set; }
        public string sku { get; set; }
        public double amount { get; set; }
        public string currency { get; set; }
    }
}