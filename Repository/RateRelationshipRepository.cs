using hiberus.Contracts;
using hiberus.Model;

namespace hiberus.Repository
{
    public class RateRelationshipRepository : RepositoryBase<RateRelationship>, IRateRelationshipRepository
    {
        public RateRelationshipRepository(RepositoryContext repositoryContext, string url)
            :base(repositoryContext, url)
        {
        }
    }
}