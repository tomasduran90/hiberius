using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Threading.Tasks;
using hiberus.Contracts;
using hiberus.Model;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace hiberus.Repository 
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class 
    {
        private string _url;
        protected RepositoryContext RepositoryContext { get; set; }
     
        public RepositoryBase (RepositoryContext repositoryContext, string url) 
        {
            this.RepositoryContext = repositoryContext;
            _url = url;
        }

        public async Task<IEnumerable<T>> FindAll () 
        {
            IEnumerable<T> rvalue = null;

            if(string.IsNullOrEmpty(_url))
            {
                rvalue = this.RepositoryContext.Set<T> ();
            }
            else
            {
                try
                {
                    rvalue = await ProxyInformation(_url);

                    this.RepositoryContext.TruncateTable<T>();

                    foreach(T item in rvalue)
                    {
                        this.RepositoryContext.Set<T>().Add(item);
                    }

                    this.RepositoryContext.SaveChanges();
                }
                catch(Exception ex)
                {
                    //TODO agregar log por ID
                    rvalue = this.RepositoryContext.Set<T> ();
                }
            }

            return rvalue;
        }

        public async Task<IEnumerable<T>> FindByCondition (Expression<Func<T, bool>> expression) 
        {
            //TODO
            if(string.IsNullOrEmpty(_url))
            {
                return this.RepositoryContext.Set<T> ().Where (expression);
            }
            else
            {
                var result = await ProxyInformation(_url);
                return result.Where (expression.Compile());
            }
        }

        public void Create (T entity) 
        {
            this.RepositoryContext.Set<T>().Add (entity);
        }

        public void Update (T entity) 
        {
            this.RepositoryContext.Set<T> ().Update (entity);
        }

        public void Delete (T entity) 
        {
            this.RepositoryContext.Set<T> ().Remove (entity);
        }

        public void Save () 
        {
            this.RepositoryContext.SaveChanges ();
        }

        private async Task<IEnumerable<T>> ProxyInformation (string url) 
        {
            using (var client = new HttpClient ()) 
            {
                try 
                {
                    var response = await client.GetAsync (url);
                    response.EnsureSuccessStatusCode ();

                    var stringResult = await response.Content.ReadAsStringAsync();
                    var webInformation = JsonConvert.DeserializeObject<IEnumerable<T>> (stringResult);
                    return webInformation;
                } 
                catch (Exception ex) 
                {
                    throw ex;
                }
            }
        }
    }
}