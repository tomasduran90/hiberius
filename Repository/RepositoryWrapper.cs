using hiberus.Configuration;
using hiberus.Contracts;
using hiberus.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace hiberus.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _repoContext;
        private IRateRelationshipRepository _rateRelationship;
        private ITransactionRepository _transaction;
        private ILogger<RepositoryWrapper> _logger;
        private IOptions<AppSettings> _config;

        public RepositoryWrapper(RepositoryContext repositoryContext, ILogger<RepositoryWrapper> logger, IOptions<AppSettings> config)
        {
            _repoContext = repositoryContext;
            _logger = logger;
            _config = config;
        }
 
        public IRateRelationshipRepository RateRelationship {
            get {
                if(_rateRelationship == null)
                {
                    _rateRelationship = new RateRelationshipRepository(_repoContext, _config.Value.RateUrl);
                }
 
                return _rateRelationship;
            }
        }
 
        public ITransactionRepository Transaction {
            get {
                if(_transaction == null)
                {
                    _transaction = new TransactionRepository(_repoContext, _config.Value.TransactionUrl);
                }
 
                return _transaction;
            }
        }
    }
}