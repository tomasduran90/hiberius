using hiberus.Contracts;
using hiberus.Model;

namespace hiberus.Repository
{
    public class TransactionRepository : RepositoryBase<Transaction>, ITransactionRepository
    {
        public TransactionRepository(RepositoryContext repositoryContext, string url)
            :base(repositoryContext, url)
        {
        }
    }
}