using hiberus.Contracts;
using hiberus.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace hiberus.Services 
{
    public static class ServicesConfiguration 
    {
        public static void ConfigureRepositoryWrapper (this IServiceCollection services) 
        {
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
        }
    }
}