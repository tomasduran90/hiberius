﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using hiberus.Services;
using hiberus.Configuration;
using System.IO;

namespace hiberus 
{
    public class Startup
    {
        public Startup (IConfiguration configuration, IHostingEnvironment env) 
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            Configuration = builder.Build();
        }

        public IConfiguration Configuration 
        { 
            get; 
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) 
        {
            
            var connection = @"Server=db,1433;Database=master;User=sa;Password=Your_password123;";

            services.AddDbContext<Model.RepositoryContext> (
                options => options.UseSqlServer (connection));

            services.ConfigureRepositoryWrapper();

            services.AddMvc ().SetCompatibilityVersion (CompatibilityVersion.Version_2_2);

            services.AddOptions();

            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IHostingEnvironment env, Model.RepositoryContext dbcontext) 
        {
            if (env.IsDevelopment ()) 
            {
                app.UseDeveloperExceptionPage ();
            } 
            else 
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts ();
            }

            //Redirect non api calls to angular app that will handle routing of the app. 
            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404 &&
                   !Path.HasExtension(context.Request.Path.Value) &&
                   !context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            UpdateDatabase(app);

            // configure the app to serve index.html from /wwwroot folder
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseHttpsRedirection ();
            app.UseMvc ();
        }

         private static void UpdateDatabase(IApplicationBuilder app)
         {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<Model.RepositoryContext>())
                {
                    context.Database.Migrate();
                }
            }
        }
    }
}