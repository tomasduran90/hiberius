﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190102205705_DBMigration')
BEGIN
    CREATE TABLE [Rate] (
        [from] nvarchar(450) NOT NULL,
        [to] nvarchar(450) NOT NULL,
        [rate] float NOT NULL,
        CONSTRAINT [PK_Rate] PRIMARY KEY ([from], [to])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190102205705_DBMigration')
BEGIN
    CREATE TABLE [Transaction] (
        [id] bigint NOT NULL IDENTITY,
        [sku] nvarchar(max) NULL,
        [amount] float NOT NULL,
        [currency] nvarchar(max) NULL,
        CONSTRAINT [PK_Transaction] PRIMARY KEY ([id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190102205705_DBMigration')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190102205705_DBMigration', N'2.2.0-rtm-35687');
END;

GO

