import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TransactionslistComponent } from './transactionslist/transactionslist.component';
import { AppMaterialModule } from './app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routing } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';
import { TransactionsService } from './services/transactions.service';
import { RatesService } from './services/rates.service';
import { TransactionsearchComponent } from './transactionsearch/transactionsearch.component';

@NgModule({
  declarations: [
    AppComponent,
    TransactionslistComponent,
    TransactionsearchComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppMaterialModule,   
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    Routing
  ],
  providers: [
    RatesService, 
    TransactionsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
