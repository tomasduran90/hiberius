import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransactionslistComponent } from './transactionslist/transactionslist.component';
import { TransactionsearchComponent } from './transactionsearch/transactionsearch.component';

const appRoutes: Routes = [
  { path: '',  pathMatch: 'full' , component: TransactionsearchComponent },
  { path: 'transactions', component: TransactionslistComponent }
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);