export interface IRate {
    from: string;
    to: string;
    rate: number;
}