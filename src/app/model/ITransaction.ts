export interface ITransaction {
    sku: string;
    amount: number;
    currency: string;
}