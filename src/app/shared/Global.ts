export class Global {
    public static BASE_RATES_ENDPOINT = 'api/rates/';
    public static BASE_TRANSACTIONS_ENDPOINT = 'api/transaction/';
    public static BASE_RATE = 'EUR';
}