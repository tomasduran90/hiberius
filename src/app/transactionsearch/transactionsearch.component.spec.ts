import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionsearchComponent } from './transactionsearch.component';

describe('TransactionsearchComponent', () => {
  let component: TransactionsearchComponent;
  let fixture: ComponentFixture<TransactionsearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionsearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
