import { Component, ViewChild, OnInit } from '@angular/core';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TransactionslistComponent } from '../transactionslist/transactionslist.component';
import { FormGroup, FormControl } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
import { strictEqual } from 'assert';

@Component({
  selector: 'app-transactionsearch',
  templateUrl: './transactionsearch.component.html',
  styleUrls: ['./transactionsearch.component.css']
})
export class TransactionsearchComponent implements OnInit {

  sku: string;
  transFrm: FormGroup;

  constructor(public snackBar: MatSnackBar, private dialog: MatDialog, private fb: FormBuilder) { }

  ngOnInit() {
    this.transFrm = this.fb.group({
      sku: ['', [Validators.required, Validators.maxLength(5)]]
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(TransactionslistComponent, {
      width: '500px',
      data: { sku: this.sku}
    });
  }

  onSubmit(formData: any) {
    if(formData.value.sku != '')
    {
      this.sku = formData.value.sku;
      this.openDialog();
    }
  }

}
