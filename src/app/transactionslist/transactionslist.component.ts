import {
  Component,
  Inject,
  OnInit,
  ChangeDetectorRef,
  ChangeDetectionStrategy
} from "@angular/core";
import { MatTableDataSource, MatSnackBar } from "@angular/material";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { TransactionsService } from "../services/transactions.service";
import { RatesService } from "../services/rates.service";
import { ITransaction } from "../model/ITransaction";
import { IRate } from "../model/IRate";
import { Global } from "../shared/Global";
import { TransactionsearchComponent } from "../transactionsearch/transactionsearch.component";

@Component({
  selector: "app-transactionslist",
  templateUrl: "./transactionslist.component.html",
  styleUrls: ["./transactionslist.component.css"]
})
export class TransactionslistComponent implements OnInit {
  transactions: ITransaction[];
  rates: IRate[];
  filterRates: IRate[];
  loadingState: boolean;
  sku: String;
  total: number;

  displayedColumns = ["sku", "amount"];
  dataSource = new MatTableDataSource<ITransaction>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public snackBar: MatSnackBar,
    private _transactionService: TransactionsService,
    private dialog: MatDialog,
    private _rateService: RatesService,
    public dialogRef: MatDialogRef<TransactionsearchComponent>
  ) {}

  ngOnInit() {
    this.loadingState = true;
    this.loadRates();
    this.sku = this.data.sku;
    this.total = 0;
  }

  loadRates(): void {
    this._rateService
      .getAllRates(Global.BASE_RATES_ENDPOINT)
      .subscribe(srates => {
        let trates = srates.map((item: IRate) => item.from);
        this.rates = new Array();

        trates.forEach(element => {
          let rate: number = this.getRate(srates, element, Global.BASE_RATE);
          if (rate != -1) {
            let nitem: IRate = {
              from: element,
              to: Global.BASE_RATE,
              rate: Math.round(rate * 1e2) / 1e2
            };
            this.rates.push(nitem);
          }
        });

        this.loadTransactions();
      });
  }

  getRate(rates: IRate[], from: string, to: string): number {
    let cambio = rates.find(x => x.from == from && x.to == to);

    if (cambio == null) {
      let newFrom = rates.filter(x => x.from == from);
      let rvalue: number = -1;

      if (newFrom == null) {
        rvalue = -1;
      } else {
        newFrom.some(element => {
          let rateResult: number = this.getRate(rates, element.to, to);

          if (rateResult != -1) {
            rvalue = element.rate * rateResult;
            return true;
          } else {
            rvalue = rateResult;
          }
        });
      }
      return rvalue;
    } else {
      return cambio.rate;
    }
  }

  toBaseRate(transaction: ITransaction) {
    var number = 0;

    if (transaction.currency == Global.BASE_RATE) {
      number = transaction.amount;
    } else {
      let rate = this.rates.find(
        r => r.from == transaction.currency && r.to == Global.BASE_RATE
      );

      if (rate) {
        number = Math.round(transaction.amount * rate.rate * 1e2) / 1e2;
      }
    }
    return number;
  }

  loadTransactions(): void {
    this._transactionService
      .getAllTransactions(Global.BASE_TRANSACTIONS_ENDPOINT)
      .subscribe(transactions => {
        this.loadingState = false;

        this.transactions = transactions
          .filter(e => e.sku == this.sku)
          .map(value => {
            value.amount = this.toBaseRate(value);
            return value;
          });

        this.total =
          Math.round(
            this.transactions
              .filter(item => item.amount)
              .map(item => +item.amount)
              .reduce((sum, current) => sum + current) * 1e2
          ) / 1e2;

        this.dataSource.data = this.transactions;
      });
  }
}
