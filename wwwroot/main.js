(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar>\n  <span>Transactions App</span>\n</mat-toolbar>\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Hiberus-App';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.material.module.ts":
/*!****************************************!*\
  !*** ./src/app/app.material.module.ts ***!
  \****************************************/
/*! exports provided: AppMaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppMaterialModule", function() { return AppMaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var AppMaterialModule = /** @class */ (function () {
    function AppMaterialModule() {
    }
    AppMaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatOptionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBarModule"]
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatOptionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBarModule"]
            ],
        })
    ], AppMaterialModule);
    return AppMaterialModule;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _transactionslist_transactionslist_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./transactionslist/transactionslist.component */ "./src/app/transactionslist/transactionslist.component.ts");
/* harmony import */ var _app_material_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.material.module */ "./src/app/app.material.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _services_transactions_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/transactions.service */ "./src/app/services/transactions.service.ts");
/* harmony import */ var _services_rates_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/rates.service */ "./src/app/services/rates.service.ts");
/* harmony import */ var _transactionsearch_transactionsearch_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./transactionsearch/transactionsearch.component */ "./src/app/transactionsearch/transactionsearch.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _transactionslist_transactionslist_component__WEBPACK_IMPORTED_MODULE_4__["TransactionslistComponent"],
                _transactionsearch_transactionsearch_component__WEBPACK_IMPORTED_MODULE_12__["TransactionsearchComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                _app_material_module__WEBPACK_IMPORTED_MODULE_5__["AppMaterialModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_9__["LayoutModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_7__["Routing"]
            ],
            providers: [
                _services_rates_service__WEBPACK_IMPORTED_MODULE_11__["RatesService"],
                _services_transactions_service__WEBPACK_IMPORTED_MODULE_10__["TransactionsService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: Routing */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Routing", function() { return Routing; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _transactionslist_transactionslist_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./transactionslist/transactionslist.component */ "./src/app/transactionslist/transactionslist.component.ts");
/* harmony import */ var _transactionsearch_transactionsearch_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./transactionsearch/transactionsearch.component */ "./src/app/transactionsearch/transactionsearch.component.ts");



var appRoutes = [
    { path: '', pathMatch: 'full', component: _transactionsearch_transactionsearch_component__WEBPACK_IMPORTED_MODULE_2__["TransactionsearchComponent"] },
    { path: 'transactions', component: _transactionslist_transactionslist_component__WEBPACK_IMPORTED_MODULE_1__["TransactionslistComponent"] }
];
var Routing = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(appRoutes);


/***/ }),

/***/ "./src/app/services/rates.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/rates.service.ts ***!
  \*******************************************/
/*! exports provided: RatesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatesService", function() { return RatesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RatesService = /** @class */ (function () {
    function RatesService(http) {
        this.http = http;
    }
    RatesService.prototype.getAllRates = function (url) {
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    };
    RatesService.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        }
        else {
            console.error("Server Error " + error.status + ", " +
                ("message: " + error.error));
        }
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])('Contact Admin.');
    };
    RatesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], RatesService);
    return RatesService;
}());



/***/ }),

/***/ "./src/app/services/transactions.service.ts":
/*!**************************************************!*\
  !*** ./src/app/services/transactions.service.ts ***!
  \**************************************************/
/*! exports provided: TransactionsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionsService", function() { return TransactionsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TransactionsService = /** @class */ (function () {
    function TransactionsService(http) {
        this.http = http;
    }
    TransactionsService.prototype.getAllTransactions = function (url) {
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
    };
    TransactionsService.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        }
        else {
            console.error("Server Error " + error.status + ", " +
                ("message: " + error.error));
        }
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])('Contact Admin.');
    };
    TransactionsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], TransactionsService);
    return TransactionsService;
}());



/***/ }),

/***/ "./src/app/shared/Global.ts":
/*!**********************************!*\
  !*** ./src/app/shared/Global.ts ***!
  \**********************************/
/*! exports provided: Global */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Global", function() { return Global; });
var Global = /** @class */ (function () {
    function Global() {
    }
    Global.BASE_RATES_ENDPOINT = 'api/rates/';
    Global.BASE_TRANSACTIONS_ENDPOINT = 'api/transaction/';
    Global.BASE_RATE = 'EUR';
    return Global;
}());



/***/ }),

/***/ "./src/app/transactionsearch/transactionsearch.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/transactionsearch/transactionsearch.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".searchcont-container, #paginator {\n    display: flex;\n    flex-direction: column;\n    align-content: center;\n    align-items: center;\n  }"

/***/ }),

/***/ "./src/app/transactionsearch/transactionsearch.component.html":
/*!********************************************************************!*\
  !*** ./src/app/transactionsearch/transactionsearch.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"searchcont-container mat-elevation-z8\" (ngSubmit)=\"onSubmit(transFrm)\"  [formGroup]=\"transFrm\">\n  <h2>Search Transaction</h2>\n  <div>\n    <mat-form-field appearance=\"outline\">\n      <mat-label>Sku</mat-label>\n      <input matInput placeholder=\"Sku\" formControlName=\"sku\">\n    </mat-form-field>\n  </div>\n  <div>\n    <button type=\"submit\" mat-raised-button color=\"primary\">Search</button>\n  </div>\n</form>"

/***/ }),

/***/ "./src/app/transactionsearch/transactionsearch.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/transactionsearch/transactionsearch.component.ts ***!
  \******************************************************************/
/*! exports provided: TransactionsearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionsearchComponent", function() { return TransactionsearchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _transactionslist_transactionslist_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../transactionslist/transactionslist.component */ "./src/app/transactionslist/transactionslist.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TransactionsearchComponent = /** @class */ (function () {
    function TransactionsearchComponent(snackBar, dialog, fb) {
        this.snackBar = snackBar;
        this.dialog = dialog;
        this.fb = fb;
    }
    TransactionsearchComponent.prototype.ngOnInit = function () {
        this.transFrm = this.fb.group({
            sku: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(5)]]
        });
    };
    TransactionsearchComponent.prototype.openDialog = function () {
        var dialogRef = this.dialog.open(_transactionslist_transactionslist_component__WEBPACK_IMPORTED_MODULE_2__["TransactionslistComponent"], {
            width: '500px',
            data: { sku: this.sku }
        });
    };
    TransactionsearchComponent.prototype.onSubmit = function (formData) {
        if (formData.value.sku != '') {
            this.sku = formData.value.sku;
            this.openDialog();
        }
    };
    TransactionsearchComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-transactionsearch',
            template: __webpack_require__(/*! ./transactionsearch.component.html */ "./src/app/transactionsearch/transactionsearch.component.html"),
            styles: [__webpack_require__(/*! ./transactionsearch.component.css */ "./src/app/transactionsearch/transactionsearch.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], TransactionsearchComponent);
    return TransactionsearchComponent;
}());



/***/ }),

/***/ "./src/app/transactionslist/transactionslist.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/transactionslist/transactionslist.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".spinner{\n    top: 45%;\n    left: 47%;\n    position: fixed;\n  }\n  .transactionslist-container, #paginator {\n    display: flex;\n    flex-direction: column;\n    overflow: auto;\n    margin: 0 auto;\n  }"

/***/ }),

/***/ "./src/app/transactionslist/transactionslist.component.html":
/*!******************************************************************!*\
  !*** ./src/app/transactionslist/transactionslist.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"spinner\" *ngIf=\"loadingState; else translist\">\n  <mat-spinner></mat-spinner>\n</div>\n<ng-template class=\"transactionslist\" #translist>\n  <mat-dialog-content>\n    <h2 style=\"text-align: center;\">Transactions for {{ sku }}</h2>\n    <h2 style=\"text-align: center;\">Total: € {{ total }}</h2>\n    <div class=\"transactionslist-container mat-elevation-z8\">\n      <table mat-table #table [dataSource]=\"dataSource\">\n        <!-- Sku Column -->\n        <ng-container matColumnDef=\"sku\">\n          <th mat-header-cell *matHeaderCellDef>Sku</th>\n          <td mat-cell *matCellDef=\"let element\">{{ element.sku }}</td>\n        </ng-container>\n\n        <!-- Amount Column -->\n        <ng-container matColumnDef=\"amount\">\n          <th mat-header-cell *matHeaderCellDef>Amount</th>\n          <td mat-cell *matCellDef=\"let element\">€ {{ element.amount }}</td>\n        </ng-container>\n\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns\"></tr>\n      </table>\n    </div>\n    <h2 style=\"text-align: left;\">Total: € {{ total }}</h2>\n  </mat-dialog-content>\n</ng-template>\n"

/***/ }),

/***/ "./src/app/transactionslist/transactionslist.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/transactionslist/transactionslist.component.ts ***!
  \****************************************************************/
/*! exports provided: TransactionslistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionslistComponent", function() { return TransactionslistComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_transactions_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/transactions.service */ "./src/app/services/transactions.service.ts");
/* harmony import */ var _services_rates_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/rates.service */ "./src/app/services/rates.service.ts");
/* harmony import */ var _shared_Global__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/Global */ "./src/app/shared/Global.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var TransactionslistComponent = /** @class */ (function () {
    function TransactionslistComponent(data, snackBar, _transactionService, dialog, _rateService, dialogRef) {
        this.data = data;
        this.snackBar = snackBar;
        this._transactionService = _transactionService;
        this.dialog = dialog;
        this._rateService = _rateService;
        this.dialogRef = dialogRef;
        this.displayedColumns = ["sku", "amount"];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"]();
    }
    TransactionslistComponent.prototype.ngOnInit = function () {
        this.loadingState = true;
        this.loadRates();
        this.sku = this.data.sku;
        this.total = 0;
    };
    TransactionslistComponent.prototype.loadRates = function () {
        var _this = this;
        this._rateService
            .getAllRates(_shared_Global__WEBPACK_IMPORTED_MODULE_4__["Global"].BASE_RATES_ENDPOINT)
            .subscribe(function (srates) {
            var trates = srates.map(function (item) { return item.from; });
            _this.rates = new Array();
            trates.forEach(function (element) {
                var rate = _this.getRate(srates, element, _shared_Global__WEBPACK_IMPORTED_MODULE_4__["Global"].BASE_RATE);
                if (rate != -1) {
                    var nitem = {
                        from: element,
                        to: _shared_Global__WEBPACK_IMPORTED_MODULE_4__["Global"].BASE_RATE,
                        rate: Math.round(rate * 1e2) / 1e2
                    };
                    _this.rates.push(nitem);
                }
            });
            _this.loadTransactions();
        });
    };
    TransactionslistComponent.prototype.getRate = function (rates, from, to) {
        var _this = this;
        var cambio = rates.find(function (x) { return x.from == from && x.to == to; });
        if (cambio == null) {
            var newFrom = rates.filter(function (x) { return x.from == from; });
            var rvalue_1 = -1;
            if (newFrom == null) {
                rvalue_1 = -1;
            }
            else {
                newFrom.some(function (element) {
                    var rateResult = _this.getRate(rates, element.to, to);
                    if (rateResult != -1) {
                        rvalue_1 = element.rate * rateResult;
                        return true;
                    }
                    else {
                        rvalue_1 = rateResult;
                    }
                });
            }
            return rvalue_1;
        }
        else {
            return cambio.rate;
        }
    };
    TransactionslistComponent.prototype.toBaseRate = function (transaction) {
        var number = 0;
        if (transaction.currency == _shared_Global__WEBPACK_IMPORTED_MODULE_4__["Global"].BASE_RATE) {
            number = transaction.amount;
        }
        else {
            var rate = this.rates.find(function (r) { return r.from == transaction.currency && r.to == _shared_Global__WEBPACK_IMPORTED_MODULE_4__["Global"].BASE_RATE; });
            if (rate) {
                number = Math.round(transaction.amount * rate.rate * 1e2) / 1e2;
            }
        }
        return number;
    };
    TransactionslistComponent.prototype.loadTransactions = function () {
        var _this = this;
        this._transactionService
            .getAllTransactions(_shared_Global__WEBPACK_IMPORTED_MODULE_4__["Global"].BASE_TRANSACTIONS_ENDPOINT)
            .subscribe(function (transactions) {
            _this.loadingState = false;
            _this.transactions = transactions
                .filter(function (e) { return e.sku == _this.sku; })
                .map(function (value) {
                value.amount = _this.toBaseRate(value);
                return value;
            });
            _this.total =
                Math.round(_this.transactions
                    .filter(function (item) { return item.amount; })
                    .map(function (item) { return +item.amount; })
                    .reduce(function (sum, current) { return sum + current; }) * 1e2) / 1e2;
            _this.dataSource.data = _this.transactions;
        });
    };
    TransactionslistComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-transactionslist",
            template: __webpack_require__(/*! ./transactionslist.component.html */ "./src/app/transactionslist/transactionslist.component.html"),
            styles: [__webpack_require__(/*! ./transactionslist.component.css */ "./src/app/transactionslist/transactionslist.component.css")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"],
            _services_transactions_service__WEBPACK_IMPORTED_MODULE_2__["TransactionsService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _services_rates_service__WEBPACK_IMPORTED_MODULE_3__["RatesService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]])
    ], TransactionslistComponent);
    return TransactionslistComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/tomasduran/Desktop/Zaragoza/TomasDuranTest/hiberius/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map